/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */package ch.fhnw.ether.examples.raytracing;

import java.util.ArrayList;
import java.util.List;

import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.EventDrivenScheduler;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.builtin.MattMaterial;
import ch.fhnw.ether.render.raytracer.materials.builtin.ReflectiveMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.CheckBoardTexture;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.MarbleTexture;
import ch.fhnw.ether.render.raytracer.objects.parametric.ParametricRayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.PlaneSurface;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.SphereSurface;
import ch.fhnw.ether.render.raytracer.renderers.AbstractRayTracingRenderer;
import ch.fhnw.ether.render.raytracer.renderers.ParallelRayTracingRenderer;
import ch.fhnw.ether.render.raytracer.shaders.PhongShader;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.I3DObject;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.PointLight;
import ch.fhnw.ether.view.IView;
import ch.fhnw.ether.view.gl.DefaultView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public class MultipleLightsExample3 extends AbstractRaytracingExample {


	public static void main(final String[] args) {
		new MultipleLightsExample3();
	}

	public MultipleLightsExample3() {
		final int superSampling = getSuperSamplingRate();
		final int softShadowSampling = getSoftShadowRate();
		final int width = getResolutionX();
		final int height = getResolutionY();
		
		// load objects
		final List<I3DObject> objects = new ArrayList<>();


		objects.add(new PointLight(new Vec3(-600, -600, 500), RGB.BLACK, RGB.RED));
		objects.add(new PointLight(new Vec3(300, -300, 500), RGB.BLACK, RGB.WHITE));
		objects.add(new PointLight(new Vec3(0, -800, 500), RGB.BLACK, RGB.BLUE));

		
		objects.add(new ParametricRayTracingObject(new Vec3(200, 200, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 200, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-200, 200, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(200, 000, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 000, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-200, 000, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(200, -200, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, -200, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-200, -200, 100), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-100, -100, 200), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-100, 100, 200), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(100, -100, 200), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(100, 100, 200), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 0, 300), new SphereSurface(100), new PhongShader(), new ReflectiveMaterial()));
		
		objects.add(new ParametricRayTracingObject(new Vec3(1000, 1000, 600), new SphereSurface(600), new PhongShader(), new ReflectiveMaterial()));
		
		objects.add(new ParametricRayTracingObject(new Vec3(0, 0, 0), new PlaneSurface(), new PhongShader(), new MattMaterial(new SimpleColorTexture(new RGB(0.6f, 0.0f, 0.4f)))));

		// create controller, camera, scene and view
		final AbstractRayTracingRenderer renderer = new ParallelRayTracingRenderer(superSampling, softShadowSampling);
		final IController controller = new DefaultController(new EventDrivenScheduler(), renderer);
		final IScene scene = new DefaultScene(controller);
		controller.setScene(scene);

		objects.stream().forEach(so->scene.add3DObject(so));

		final Vec3 cameraPosition = new Vec3(0, -800, 600);
		final Vec3 targetPosition = new Vec3(0, 0, 0);
		final Vec3 look = targetPosition.subtract(cameraPosition).normalize();
		final Vec3 rightVector = look.cross(Vec3.Y);
		final Vec3 up = look.cross(rightVector).scale(-1);
		final ICamera camera = new Camera(cameraPosition, targetPosition, up, 2.5f, 0.5f, Float.POSITIVE_INFINITY);
		final IView view = new DefaultView(controller, 50, 50, width, height, IView.MAPPED_VIEW, ExampleUtil.getWindowTitle(getClass().getName(), superSampling, softShadowSampling), camera);
		controller.addView(view);
	}
}
