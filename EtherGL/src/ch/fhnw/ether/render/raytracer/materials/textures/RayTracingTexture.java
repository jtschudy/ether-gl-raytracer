package ch.fhnw.ether.render.raytracer.materials.textures;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

public class RayTracingTexture implements IRayTracingTexure {

	private BufferedImage image;
	private boolean scale;

	public RayTracingTexture(URL url, boolean doScale) {
		this.scale = doScale;
		try {
			image = ImageIO.read(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public RGB getPixel(Vec3 origin, Vec2 mapped, int w, int h) {
		final int x = (int) ((Math.abs(mapped.x) * (scale ? image.getWidth() : 1)) % image.getWidth());
		final int y = (int) ((Math.abs(mapped.y) * (scale ? image.getHeight() : 1)) % image.getHeight());

		final int color = image.getRGB(x, y);
		final int blue = color & 0xff;
		final int green = (color & 0xff00) >> 8;
		final int red = (color & 0xff0000) >> 16;

		return new RGB(red / 255f, green / 255f, blue / 255f);
	}

}
