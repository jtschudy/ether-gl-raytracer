package ch.fhnw.ether.render.raytracer.objects;

import java.util.Optional;

import ch.fhnw.ether.render.raytracer.IntersectionResult;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.shaders.IRayTracingShader;
import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.material.IMaterial;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.Line;

public abstract class RayTracingObject implements IMesh {
	protected final IRayTracingMaterial material;
	protected final IRayTracingShader shader;

	protected Vec3 position;

	public RayTracingObject(Vec3 position, IRayTracingMaterial material, IRayTracingShader shader) {
		super();
		this.position = position;
		this.material = material;
		this.shader = shader;
	}

	public IRayTracingMaterial getRayTracingMaterial() {
		return material;
	}

	@Override
	public final IMaterial getMaterial() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Returns the diffuse and specular color for the given position.
	 * 
	 * @param intersection
	 * @param rayOrigin
	 * @param intersectionNormal
	 * @param light
	 * @param w
	 * @param h
	 * @return
	 */
	public Vec3 shade(Vec3 intersection, Vec3 rayOrigin, Vec3 intersectionNormal, ILight light, int w, int h) {
		final Vec2 textureCoordinates = mapTexture(intersection);
		final RGB pixelColor = material.getTexture().getPixel(intersection, textureCoordinates, w, h);

		return shader.apply(pixelColor, material.getSpecular(), rayOrigin, intersection, intersectionNormal, light);
	}

	/**
	 * Map the given coordinates on the mesh to texture coordinates.
	 * 
	 * @param v
	 * @return
	 */
	public abstract Vec2 mapTexture(Vec3 v);

	/**
	 * Returns the {@link IntersectionResult} of this mesh with the given {@link Line}. Or an empty Optional otherwise.
	 * 
	 * @param line
	 * @return
	 */
	public abstract Optional<IntersectionResult> intersect(Line line);

	@Override
	public void setPosition(Vec3 position) {
		this.position = position;
	}

	@Override
	public Vec3 getPosition() {
		return position;
	}

}
