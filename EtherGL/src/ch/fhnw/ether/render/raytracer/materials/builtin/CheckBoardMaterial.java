package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.CheckBoardTexture;
import ch.fhnw.util.color.RGB;

public class CheckBoardMaterial implements IRayTracingMaterial {

	private final CheckBoardTexture texture;

	public CheckBoardMaterial(int squareSize, int scale) {
		texture = new CheckBoardTexture(squareSize, scale);
	}

	@Override
	public float getRefractiveIndex() {
		return 1;
	}

	@Override
	public float getReflection() {
		return 0;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0, 0f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return texture;
	}

	@Override
	public NormalMap getNormalMap() {
		return null;
	}

}
