package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.util.color.RGB;

public class TexturedMaterial implements IRayTracingMaterial {

	final SimpleColorTexture colorTexture;

	public TexturedMaterial(final RGB rgb) {
		colorTexture = new SimpleColorTexture(rgb);
	}

	@Override
	public float getRefractiveIndex() {
		return 1f;
	}

	@Override
	public float getReflection() {
		return 0.0f;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0f, 0f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return colorTexture;
	}

	@Override
	public NormalMap getNormalMap() {
		// TODO Auto-generated method stub
		return null;
	}

}
