package ch.fhnw.ether.render.raytracer.objects;

import java.util.EnumSet;
import java.util.Optional;

import ch.fhnw.ether.render.raytracer.IntersectionResult;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.shaders.IRayTracingShader;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.BoundingBox;
import ch.fhnw.util.math.geometry.Line;

public class MeshRayTracingObject extends RayTracingObject {
	private DefaultMesh defaultMesh;

	private static final float EPSILON = 0.00001f;

	public MeshRayTracingObject(Vec3 position, DefaultMesh defaultMesh, IRayTracingShader shader, IRayTracingMaterial material) {
		super(position, material, shader);
		this.defaultMesh = defaultMesh;
	}

	@Override
	public int hashCode() {
		return defaultMesh.hashCode();
	}

	@Override
	public BoundingBox getBounds() {
		return defaultMesh.getBounds();
	}

	@Override
	public Vec3 getPosition() {
		return defaultMesh.getPosition();
	}

	@Override
	public void setPosition(Vec3 position) {
		defaultMesh.setPosition(position);
	}

	@Override
	public String getName() {
		return defaultMesh.getName();
	}

	@Override
	public void setName(String name) {
		defaultMesh.setName(name);
	}

	@Override
	public Pass getPass() {
		return defaultMesh.getPass();
	}

	@Override
	public boolean equals(Object obj) {
		return defaultMesh.equals(obj);
	}

	@Override
	public EnumSet<Flags> getFlags() {
		return defaultMesh.getFlags();
	}

	@Override
	public IGeometry getGeometry() {
		return defaultMesh.getGeometry();
	}

	@Override
	public boolean needsUpdate() {
		return defaultMesh.needsUpdate();
	}

	@Override
	public void requestUpdate(Object source) {
		defaultMesh.requestUpdate(source);
	}

	@Override
	public String toString() {
		return defaultMesh.toString();
	}

	@Override
	public Optional<IntersectionResult> intersect(final Line ray) {
		// ray triangle intersection:
		// http://geomalgorithms.com/a06-_intersect-2.html
		return defaultMesh.getGeometry().find((attributes, data) -> {

			IntersectionResult result = null;

			for (int triangle = 0; triangle < data[0].length; triangle += 9) {
				final Vec3 v0 = new Vec3(data[0][triangle + 0], data[0][triangle + 1], data[0][triangle + 2]);
				final Vec3 v1 = new Vec3(data[0][triangle + 3], data[0][triangle + 4], data[0][triangle + 5]);
				final Vec3 v2 = new Vec3(data[0][triangle + 6], data[0][triangle + 7], data[0][triangle + 8]);

				final Vec3 u = v1.subtract(v0);
				final Vec3 v = v2.subtract(v0);
				final Vec3 n = u.cross(v).normalize();

				final Vec3 dir = ray.getDirection();
				final Vec3 w0 = ray.getOrigin().subtract(v0);
				final float a = -n.dot(w0);
				final float b = n.dot(dir);

				// ray parallel to triangle
				if (Math.abs(b) < EPSILON) {
					continue;
				}

				final float r = a / b;

				// ray goes away from triangle
				if (r < 0) {
					continue;
				}

				final Vec3 I = ray.getOrigin().add(ray.getDirection().scale(r));
				final float uu = u.dot(u);
				final float uv = u.dot(v);
				final float vv = v.dot(v);
				final Vec3 w = I.subtract(v0);
				final float wu = w.dot(u);
				final float wv = w.dot(v);

				final float D = uv * uv - uu * vv;
				final float s = (uv * wv - vv * wu) / D;
				if (s < 0.0 || s > 1)
					continue;

				final float t = (uv * wu - uu * wv) / D;
				if (t < 0.0 || (s + t) > 1.0)
					continue;

				if (result == null || r < result.dist) {
					result = new IntersectionResult(MeshRayTracingObject.this, I, n, r);
				}
			}

			return Optional.ofNullable(result);
		});

	}

	@Override
	public Vec2 mapTexture(Vec3 v) {
		// TODO implement texture mapping for arbitrary 3d object
		return new Vec2(0, 0);
	}

}
