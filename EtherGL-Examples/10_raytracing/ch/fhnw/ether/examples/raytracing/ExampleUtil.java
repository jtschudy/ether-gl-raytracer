package ch.fhnw.ether.examples.raytracing;

public class ExampleUtil {

	public static String getWindowTitle(String prefix, final int superSampling, final int softShadowSampling) {
		return prefix + "\t AA: " + superSampling + " soft-Shadows: " + softShadowSampling;
	}
}
