package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.util.color.RGB;

public class MattMaterial implements IRayTracingMaterial {

	final SimpleColorTexture colorTexture;
	final float reflection;
	
	public MattMaterial(){
		this(0.02f);
	}

	public MattMaterial(float reflection) {
		this(new SimpleColorTexture(new RGB(0.2f, 0.2f, 0.6f)), reflection);
	}	

	public MattMaterial(SimpleColorTexture colorTexture) {
		this(colorTexture, 0.2f);
	}

	public MattMaterial(SimpleColorTexture colorTexture, final float reflection) {
		this.colorTexture = colorTexture;
		this.reflection = reflection;
	}

	@Override
	public float getRefractiveIndex() {
		return 1f;
	}

	@Override
	public float getReflection() {
		return this.reflection;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0f, 0.2f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return colorTexture;
	}

	@Override
	public NormalMap getNormalMap() {
		// TODO Auto-generated method stub
		return null;
	}
}