package ch.fhnw.ether.render.raytracer.materials.builtin;

import java.net.URL;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.RayTracingTexture;
import ch.fhnw.util.color.RGB;

public class EarthMaterial implements IRayTracingMaterial {
	private final RayTracingTexture texture;
	final static URL resource = EarthMaterial.class.getClassLoader().getResource("ch/fhnw/ether/render/raytracer/materials/builtin/assets/earth_nasa.jpg");	

	public EarthMaterial() {
		texture = new RayTracingTexture(resource, true);
	}

	@Override
	public float getRefractiveIndex() {
		return 1;
	}

	@Override
	public float getReflection() {
		return 0;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0, 0f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return texture;
	}

	@Override
	public NormalMap getNormalMap() {
		return null;
	}

}