package ch.fhnw.ether.render.raytracer.renderers;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL3;

import ch.fhnw.ether.render.attribute.base.UniformBlockAttribute;
import ch.fhnw.ether.render.attribute.base.Vec3FloatUniformAttribute;
import ch.fhnw.ether.render.attribute.builtin.ColorArray;
import ch.fhnw.ether.render.attribute.builtin.PositionArray;
import ch.fhnw.ether.render.attribute.builtin.ProjMatrixUniform;
import ch.fhnw.ether.render.attribute.builtin.ViewMatrixUniform;
import ch.fhnw.ether.render.forward.ForwardRenderer;
import ch.fhnw.ether.render.gl.UniformBuffer;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.CheckBoardTexture;
import ch.fhnw.ether.render.raytracer.objects.RayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.ParametricRayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.IParametricSurface;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.PlaneSurface;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.SphereSurface;
import ch.fhnw.ether.render.shader.base.AbstractShader;
import ch.fhnw.ether.scene.attribute.IAttribute;
import ch.fhnw.ether.scene.attribute.IAttributeProvider;
import ch.fhnw.ether.scene.camera.CameraMatrices;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.IMesh.Pass;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry.Primitive;
import ch.fhnw.ether.scene.mesh.material.CustomMaterial;
import ch.fhnw.ether.scene.mesh.material.IMaterial;
import ch.fhnw.ether.view.IView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

// initial implementation based on https://www.shadertoy.com/view/4dsGRn
public class GPURayTracingRenderer extends ForwardRenderer {

	private final UniformBuffer uniformBuffer = new UniformBuffer(1);

	private static final int MAX_OBEJCTS = 120;
	private static final int BLOCK_SIZE = 20;
	private static final float[] NO_OBJECT = new float[BLOCK_SIZE];

	public static final float SPHERE_OBJECT = 1;
	public static final float PLANE_OBJECT = 2;

	public static final float CHECKBOARD_MATERIAL = 0;

	public static final String SCENE_DATA_ID = "sceneData";
	public static final String RESOLUTION_ID = "resolution";

	private final List<RayTracingObject> objects = new ArrayList<>();

	public static class RayTracingShader extends AbstractShader {

		public RayTracingShader() {
			super(GPURayTracingRenderer.class, "gpu_raytracing_renderer.ray_tracer_shader", "raytracing_shader", Primitive.TRIANGLES);
			addArray(new PositionArray());
			addArray(new ColorArray());

			addUniform(new ProjMatrixUniform());
			addUniform(new ViewMatrixUniform());

			addUniform(new Vec3FloatUniformAttribute(RESOLUTION_ID, RESOLUTION_ID));
			addUniform(new UniformBlockAttribute(SCENE_DATA_ID, SCENE_DATA_ID));
		}
	}

	public GPURayTracingRenderer(final int width, final int height) {

		addAttributeProvider(new IAttributeProvider() {
			@Override
			public void getAttributeSuppliers(final ISuppliers suppliers) {
				suppliers.provide(SCENE_DATA_ID, () -> 1);
			}
		});

		addAttributeProvider(new IAttributeProvider() {
			@Override
			public void getAttributeSuppliers(final ISuppliers suppliers) {
				suppliers.provide(RESOLUTION_ID, () -> new Vec3(width, height, 0));
			}
		});

		final IMesh screenPlaneMesh = createScreenPlane(-1, -1, 2, 2);

		// add to renderables
		super.addMesh(screenPlaneMesh);
	}
	
	@Override
	public void render(GL3 gl, IView view) {
		super.render(gl, view);
	}

	@Override
	protected void update(final GL3 gl, final CameraMatrices cameraMatrices) {
		
		final FloatBuffer buffer = FloatBuffer.allocate(MAX_OBEJCTS * BLOCK_SIZE);

		// pass data to shader
		for (final RayTracingObject object : objects) {
			if (object instanceof ParametricRayTracingObject) {
				final ParametricRayTracingObject parametricRayTracingObject = (ParametricRayTracingObject) object;
				final IParametricSurface surface = parametricRayTracingObject.getSurface();

				if (surface instanceof SphereSurface) {
					final SphereSurface sphere = (SphereSurface) surface;
					buffer.put(parametricRayTracingObject.getPosition().toArray()); // position
					buffer.put(SPHERE_OBJECT);
					buffer.put(new float[] { sphere.getR(), 0, 0, 0 }); // attributes
				} else if (surface instanceof PlaneSurface) {
					final PlaneSurface plane = (PlaneSurface) surface;
					buffer.put(plane.getNormal().toArray()); // position
					buffer.put(PLANE_OBJECT);
					buffer.put(new float[] { plane.getDistance(), 0, 0, 0 }); // attributes

				} else {
					throw new IllegalStateException();
				}

				final IRayTracingMaterial rayTracingMaterial = parametricRayTracingObject.getRayTracingMaterial();

				// diffuse color
				int materialType = 0;
				final IRayTracingTexure texture = rayTracingMaterial.getTexture();
				if (texture instanceof SimpleColorTexture) {
					final SimpleColorTexture colorTexture = (SimpleColorTexture) texture;
					final RGB diffuseColor = colorTexture.getColor();
					buffer.put(diffuseColor.toArray());
					buffer.put(0); // pad
				} else if (texture instanceof CheckBoardTexture) {
					materialType = 1;
				} else {
					throw new IllegalStateException();
				}

				// specular color
				final RGB specular = rayTracingMaterial.getSpecular();
				buffer.put(specular.toArray());
				buffer.put(0); // pad
				buffer.put(rayTracingMaterial.isTransparent() ? 1 : 0);
				buffer.put(rayTracingMaterial.getRefractiveIndex());
				buffer.put(rayTracingMaterial.getReflection());
				buffer.put(materialType);
			} else {
				throw new IllegalStateException();
			}
		}

		for (int i = 0; i < MAX_OBEJCTS - objects.size(); ++i) {
			buffer.put(NO_OBJECT);
		}

		uniformBuffer.load(gl, buffer);
		uniformBuffer.bind(gl);
		
		super.update(gl, cameraMatrices);
	}

	@Override
	public void addMesh(final IMesh mesh) {
		if (mesh instanceof RayTracingObject)
			objects.add((RayTracingObject) mesh);
	}

	@Override
	public void removeMesh(final IMesh mesh) {
		objects.remove(mesh);
	}

	private static IMesh createScreenPlane(final float x, final float y, final float w, final float h) {
		final IAttribute[] attribs = { IMaterial.POSITION_ARRAY, IMaterial.COLOR_ARRAY };
		final float[] position = { x, y, 0, x + w, y, 0, x + w, y + h, 0, x, y, 0, x + w, y + h, 0, x, y + h, 0 };
		final float[] color = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		final float[][] data = { position, color };
		final IGeometry geometry = new DefaultGeometry(Primitive.TRIANGLES, attribs, data);

		return new DefaultMesh(new CustomMaterial(new RayTracingShader()), geometry, Pass.DEVICE_SPACE_OVERLAY);
	}
}
