package ch.fhnw.ether.render.raytracer.materials;

import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.util.color.RGB;

public interface IRayTracingMaterial {

	/**
	 * Returns the {@link NormalMap} of this material
	 * @return
	 */
	public NormalMap getNormalMap();
	
	/**
	 * Returns the refractive index of this material.
	 * 
	 * @return
	 */
	public float getRefractiveIndex();

	/**
	 * Returns the amount of light refracted by this material.
	 * 
	 * @return
	 */
	public boolean isTransparent();

	/**
	 * Returns the amount of light reflected by this material.
	 * 
	 * @return
	 */
	public float getReflection();

	/**
	 * Returns the diffuse color of this material.
	 * 
	 * @return
	 */
	public RGB getSpecular();

	// TODO implement shininess in PhongShader
	/**
	 * Returns the shininess of this material.
	 * 
	 * @return
	 */
	public float getShininess();

	// TODO re add getDiffuse() and remove SimpleColorTexture
	/**
	 * Returns the texture of this material.
	 * 
	 * @return
	 */
	public IRayTracingTexure getTexture();

}
