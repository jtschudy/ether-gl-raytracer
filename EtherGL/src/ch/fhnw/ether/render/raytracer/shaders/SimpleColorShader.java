package ch.fhnw.ether.render.raytracer.shaders;

import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

// TODO to not use shader for this but rather add diffuse color directly to the material
public class SimpleColorShader implements IRayTracingShader {

	@Override
	public RGB apply(RGB diffuseRgb, RGB specularRgb, Vec3 origin, Vec3 position, Vec3 normal, ILight light) {
		return new RGB(diffuseRgb);
	}

}
