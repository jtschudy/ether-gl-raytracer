/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package ch.fhnw.ether.render.raytracer.renderers;

import java.util.ArrayList;
import java.util.List;

import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Vec3;

public class ParallelRayTracingRenderer extends AbstractRayTracingRenderer {

	private static final int NUMBER_OF_THREADS = Runtime.getRuntime().availableProcessors();
	private long progress = 0;
	private long percentage = 0;

	public ParallelRayTracingRenderer(int superSamplingRate, int shadowSamplingRate) {
		super(superSamplingRate, shadowSamplingRate);
	}

	@Override
	protected void internalRender(List<? extends ILight> lights, Vec3 camPos, float deltaX, float deltaY, Vec3 lookVector, Vec3 upVector, Vec3 sideVector) {
		System.out.println("Number of Threads: " + NUMBER_OF_THREADS);
		final long totalPixels = w * h;
		final List<Thread> workers = new ArrayList<>();
		progress = 0;
		percentage = 0;

		for (int i = 0; i < NUMBER_OF_THREADS; i++) {
			final int fromPx = (-h / 2) + i * (h / NUMBER_OF_THREADS);
			final int toPx = (-h / 2) + (i + 1) * (h / NUMBER_OF_THREADS);
			System.out.println("Thread: " + i + "\tfrom: " + fromPx + "\tto:" + toPx);
			workers.add(new Thread(new Runnable() {
				@Override
				public void run() {
					for (int j = fromPx; j < toPx; ++j) {
						for (int i = -w / 2; i < w / 2; ++i) {
							final RGBA samplingColor = sampleRay(lights, camPos, deltaX, deltaY, lookVector, upVector, sideVector, j, i);

							synchronized (frame) {
								if (progress++ % (totalPixels / 100) == 0) {
									System.out.println(++percentage + "%");
								}
								frame.setRGBA((i + w / 2), (j + h / 2), samplingColor.toRGBA());
							}
						}
					}
				}
			}));
		}

		for (Thread thread : workers) {
			thread.start();
		}
		for (Thread thread : workers) {
			try {
				synchronized (thread) {
					if (thread.isAlive())
						thread.join();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("ParallelRayTracingRenderer finished. Threads: " + NUMBER_OF_THREADS);

	}

}
