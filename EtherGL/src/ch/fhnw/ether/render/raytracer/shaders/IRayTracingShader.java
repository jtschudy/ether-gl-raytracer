package ch.fhnw.ether.render.raytracer.shaders;

import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.MathUtil;
import ch.fhnw.util.math.Vec3;

public interface IRayTracingShader {

	/**
	 * Applies this shader with the given attributes.
	 * 
	 * @param diffuseRgb
	 * @param specularRgb
	 * @param origin
	 * @param position
	 * @param normal
	 * @param light
	 * @return
	 */
	public RGB apply(RGB diffuseRgb, RGB specularRgb, Vec3 origin, Vec3 position, Vec3 normal, ILight light);

	/**
	 * Clamps the given float value between 0 and 1.
	 * 
	 * @param f
	 * @return
	 */
	public static float saturate(float f) {
		return MathUtil.clamp(f, 0.0f, 1.0f);
	}
}
