#version 140

#ifdef GL_ES
precision mediump float;
#endif

const float ZMAX = 99999.0;
const float EPSILON = 0.001;
const int MAX_BOUNCES = 3; 

const int MAX_SCENE_OBJECTS = 120;

uniform vec3 resolution;

struct SceneObject {
	vec3 v;
	float type; // 0 = none, 1 = sphere, 2 = plane, 3 = light
	vec4 customAttributes;
	vec3 diffuseColor;
	float pad0;
	vec3 specularColor;
	float pad1;
	float isTransparent; // 0 = false, 1 = true
	float refractiveIndex;
	float reflection;
	float materialType; // 0 = none, 1 = checkboard
};

layout (std140) uniform sceneData {
	SceneObject objects[MAX_SCENE_OBJECTS];
};

struct Intersection {
	SceneObject obj;

	vec3 p;
	float dist;
	
	vec3 n;
	vec3 diffuse;
	vec3 specular;
};
	
struct Ray {
	vec3 o;
	vec3 dir;
};
	
struct Light {
	vec3 p;
	vec3 color;
	float radius;
};
	
struct Plane {
	vec3 n;
	float d;
};
	
struct Sphere {
	vec3 c;
	float r;
};
	
float saturate(float f) {
	return clamp(f,0.2,1.0);
}

vec3 saturate(vec3 v) {
	return clamp(v,vec3(0.2,0.2,0.2),vec3(1,1,1));
}

Intersection RaySphere(Ray ray, Sphere sphere) {
	Intersection i;
	i.dist = ZMAX;
	vec3 c = sphere.c;
	float r = sphere.r;
	vec3 e = c-ray.o;
	float a = dot(e, ray.dir);
	float b = r*r - dot(e,e) + a*a;
	if(b > 0.0) {
		float f = sqrt(b);
		float t = a - f;
		if(t > EPSILON) {
			i.p = ray.o + ray.dir*t;
			i.n = normalize(i.p-c);
			i.dist = t;	
		}
	}
	return i;
}

Intersection RayPlane(Ray ray, Plane p) {
	Intersection i;
	float num = p.d-dot(p.n, ray.o);
	float denom = dot(p.n, ray.dir);
	float t = num/denom;
	if(t > EPSILON) {
		i.p = ray.o + ray.dir * t;
		i.n = p.n;
		i.dist = t;
	} else {
		i.dist = ZMAX;
	}
	return i;
}

Intersection MinIntersection(Intersection a, Intersection b) {
	if(a.dist < b.dist) {
		return a;
	} else {
		return b;
	}
}

vec3 PlaneMaterial(Intersection i) {
	/*
	float d = 0.0;
	d = mod(floor(i.p.x)+floor(i.p.z),2.0);
	return vec3(d,d,d)*0.8;
	*/
	return vec3(0,0,0);
}

Intersection SceneIntersection(Ray r) {
	Intersection minIntersection;
	
	minIntersection.dist = ZMAX;
	
	for (int objectIndex = 0; objectIndex < MAX_SCENE_OBJECTS; ++objectIndex) {
		SceneObject obj = objects[objectIndex];
		if (obj.type == 0) {
			continue;
		} else if (obj.type == 1) {
			Sphere sphere;
			
			sphere.c = obj.v;
			sphere.r = obj.customAttributes.x;
			
			Intersection sphereIntersection = RaySphere(r, sphere);
			sphereIntersection.diffuse = obj.diffuseColor;
			sphereIntersection.specular = obj.specularColor;
			sphereIntersection.obj = obj;
			
			minIntersection = MinIntersection(minIntersection, sphereIntersection);
		} else if (obj.type == 2) {
			Plane plane;
			
			plane.n = obj.v;
			plane.d = obj.customAttributes.x;
			
			Intersection planeIntersection = RayPlane(r, plane);
			planeIntersection.diffuse = PlaneMaterial(planeIntersection);
			planeIntersection.specular = vec3(1,1,1) - planeIntersection.diffuse;
			planeIntersection.obj = obj;
				
			minIntersection = MinIntersection(minIntersection, planeIntersection);		
		}
	}
	
	return minIntersection;
}

vec3 CalcLighting(Light light, Intersection i, vec3 origin) {
	vec3 n = i.n;
	vec3 p = i.p;
	vec3 l = normalize(light.p-p);
	vec3 v = normalize(origin-p);
	vec3 h = normalize(l+v);
	float NdotL = saturate(dot(n,l));
	float NdotH = saturate(dot(n,h));
	vec3 diffuse = NdotL*i.diffuse;
	vec3 spec = pow(NdotH,8.0) * i.specular;
	float distA = 1.0 - saturate(length(light.p-p) / light.radius);
	vec3 color;
	color = (diffuse + spec) * distA * light.color;
	
	float shadow = 1.0;
	Ray shadowRay;
	shadowRay.o = i.p;
	float lightDist = length(light.p - i.p);
	shadowRay.dir = (light.p - i.p) / lightDist;
	Intersection shadowI = SceneIntersection(shadowRay);
	if(shadowI.dist < lightDist) {
		shadow = 0;
	}
	color *= shadow;
	
	return color;
}

vec3 GetColor(Intersection intersection, vec3 origin) {
	vec3 color = vec3(0,0,0);
	Light light;
	
	// TODO get from uniform
	light.p = vec3(1.5,1,5);
	light.color = vec3(1,1,1);
	light.radius = 50.0;
	
	color += CalcLighting(light, intersection, origin);
	
	return color;
}

vec3 CastRay(Ray ray) {
	vec3 color = vec3(0,0,0);

	// find intersection
	Intersection intersection = SceneIntersection(ray);
	vec3 intersectionColor;
	
	if(intersection.dist >= ZMAX) {
		return color;
	} else {
		intersectionColor = GetColor(intersection, ray.o);
		color += intersectionColor;
		
		if (intersection.obj.reflection > 0) {
			vec3 incident = normalize(intersection.p - ray.o);
			ray.dir = reflect(incident, intersection.n);
			ray.o = intersection.p + ray.dir * EPSILON;
			
			// reflect 1
			intersection = SceneIntersection(ray);
			
			if(intersection.dist < ZMAX) {
				intersectionColor = GetColor(intersection, ray.o) * 0.4; // intersection.obj.reflection is not large enough? 
				color += intersectionColor;
				
				vec3 incident = normalize(intersection.p - ray.o);
				ray.dir = reflect(incident, intersection.n);
				ray.o = intersection.p + ray.dir * EPSILON;
			
				// reflect 2
				intersection = SceneIntersection(ray);
				if(intersection.dist < ZMAX) {
					intersectionColor = GetColor(intersection, ray.o) * 0.4; // intersection.obj.reflection is not large enough? 
					color += intersectionColor;
				}
			}
		}
	}
	
	return color;
}

void main( void ) {
	// position on screen
	vec2 pos = -1.0 + 2.0 * ( gl_FragCoord.xy / resolution.xy );
	
	float xOff = 2 / resolution.x;
	float yOff = 2 / resolution.y;
	
	vec2 posAR;
	posAR.x = pos.x * (resolution.x / resolution.y);
	posAR.y = pos.y;
	vec3 rayDir = normalize(vec3(posAR.x, posAR.y, 1.0));
	
	// create ray
	Ray ray;
	ray.o = vec3(0, 0, 0);
	ray.dir = rayDir;
	
	gl_FragColor = vec4(CastRay(ray), 1.0);
}