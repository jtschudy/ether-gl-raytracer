/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package ch.fhnw.ether.render.raytracer.objects.parametric.surface;

import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.Line;

public class SphereSurface implements IParametricSurface {

	private final float r;

	public SphereSurface(final float radius) {
		this.r = radius;
	}

	// From http://www.trenki.net/files/Raytracing1.pdf
	@Override
	public Vec3 intersect(final Vec3 pos, final Line ray) {
		final Vec3 o = ray.getOrigin();
		final Vec3 d = ray.getDirection();
		final Vec3 c = pos;

		final Vec3 l = c.subtract(o);
		float t;
		final float r2 = r * r;
		final float s = l.dot(d);
		final float l2 = l.dot(l);
		if (s < 0 && l2 > r2)
			return null;
		final float m2 = l2 - s * s;
		if (m2 > r2)
			return null;
		final float q = (float) Math.sqrt(r2 - m2);
		if (l2 > r2)
			t = s - q;
		else
			t = s + q;
		return o.add(d.scale(t));
	}

	@Override
	public Vec3 getNormalAt(final Vec3 surfacePosition, final Vec3 position) {
		return position.subtract(surfacePosition).normalize();
	}

	public float getR() {
		return r;
	}

	// from: https://www.cs.unc.edu/~rademach/xroads-RT/RTarticle.html
	@Override
	public Vec2 mapTexture(final Vec3 surfacePosition, final Vec3 p) {
		final Vec3 vn = new Vec3(0, 1, 0);
		final Vec3 ve = new Vec3(1, 0, 0);
		final Vec3 vp = p.subtract(surfacePosition).normalize();
		final double phi = Math.acos(-vn.dot(vp));
		final double v = phi / Math.PI;
		final double theta = (Math.acos(vp.dot(ve) / Math.sin(phi))) / (2 * Math.PI);
		double u;
		if (vn.cross(ve).dot(vp) > 0)
			u = theta;
		else
			u = 1 - theta;

		return new Vec2(u, v);

	}

}
