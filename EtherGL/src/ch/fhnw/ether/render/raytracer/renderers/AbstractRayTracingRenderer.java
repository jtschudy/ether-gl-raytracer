package ch.fhnw.ether.render.raytracer.renderers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import javax.media.opengl.GL3;

import ch.fhnw.ether.image.RGBA8Frame;
import ch.fhnw.ether.render.IRenderer;
import ch.fhnw.ether.render.forward.ForwardRenderer;
import ch.fhnw.ether.render.raytracer.IntersectionResult;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.objects.RayTracingObject;
import ch.fhnw.ether.scene.attribute.IAttribute;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.AreaLight;
import ch.fhnw.ether.scene.light.GenericLight;
import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.ether.scene.light.PointLight;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.IMesh.Pass;
import ch.fhnw.ether.scene.mesh.MeshLibrary;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry.Primitive;
import ch.fhnw.ether.scene.mesh.material.ColorMapMaterial;
import ch.fhnw.ether.scene.mesh.material.IMaterial;
import ch.fhnw.ether.scene.mesh.material.Texture;
import ch.fhnw.ether.view.IView;
import ch.fhnw.util.Viewport;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.Line;

public abstract class AbstractRayTracingRenderer implements IRenderer {

	private final List<RayTracingObject> objects = new ArrayList<>();
	protected final ForwardRenderer renderer = new ForwardRenderer();
	private final Texture screenTexture = new Texture();
	protected final IMesh plane = createScreenPlane(-1, -1, 2, 2, screenTexture);
	protected RGBA8Frame frame;
	protected int w = 0;
	protected int h = 0;
	private long n = 0;

	private static final int MAX_BOUNCES = 5;
	private static final float EPSILON = 0.1f;
	private static final float AMBIENT_FACTOR = 0.25f;

	protected final int supersamlingRate;
	protected final int shadowSamplingRate;

	private static final boolean DEBUG = true;
	private final AtomicLong intersectionChecks = new AtomicLong(0);
	private final AtomicLong raysCast = new AtomicLong(0);

	private boolean rendered;

	public AbstractRayTracingRenderer(final int superSamplingRate, final int shadowSamplingRate) {
		this.shadowSamplingRate = shadowSamplingRate;
		this.supersamlingRate = superSamplingRate;
		this.renderer.addMesh(plane);
	}

	@Override
	public void render(final GL3 gl, final IView view) {
		final Viewport viewport = view.getViewport();
		if (!rendered) {
			final long t = System.currentTimeMillis();
			if (viewport.w != w || viewport.h != h) {
				w = viewport.w;
				h = viewport.h;
				frame = new RGBA8Frame(w, h);
			}
			final ICamera camera = view.getCamera();
			final List<? extends ILight> lights = view.getController().getScene().getLights();
			final Vec3 camPos = camera.getPosition();

			final float planeWidth = (float) (2 * Math.tan(camera.getFov() / 2) * camera.getNear());
			final float planeHeight = planeWidth / viewport.getAspect();

			final float deltaX = planeWidth / w;
			final float deltaY = planeHeight / h;

			final Vec3 lookVector = camera.getTarget().subtract(camera.getPosition()).normalize();
			final Vec3 upVector = camera.getUp().normalize();
			final Vec3 sideVector = lookVector.cross(upVector).normalize();

			internalRender(lights, camPos, deltaX, deltaY, lookVector, upVector, sideVector);

			screenTexture.setData(frame);

			renderer.render(gl, view);
			System.out.println((System.currentTimeMillis() - t) + "ms for " + ++n + "th frame");

			if (DEBUG) {
				System.out.println("intersection checks: " + intersectionChecks.get());
				System.out.println("rays cast: " + raysCast.get());
			}
			rendered = true;
		} else {
			renderer.render(gl, view);
		}
	}

	@Override
	public void addMesh(final IMesh mesh) {
		if (mesh instanceof RayTracingObject)
			objects.add((RayTracingObject) mesh);
	}

	@Override
	public void removeMesh(final IMesh mesh) {
		objects.remove(mesh);
	}

	private Line reflect(final Vec3 incident, final IntersectionResult intersection) {
		final Vec3 I = incident;
		final Vec3 N = intersection.normal.normalize();

		final Vec3 dir = I.subtract(N.scale(2 * N.dot(I)));
		final Vec3 p = intersection.position.add(dir.scale(EPSILON));
		return new Line(p, dir);
	}

	/**
	 * Samples the given pixel (i, j) with the given sampling rate.
	 * 
	 * @param light
	 * @param superSampling
	 * @param camPos
	 * @param deltaX
	 * @param deltaY
	 * @param lookVector
	 * @param upVector
	 * @param sideVector
	 * @param j
	 * @param i
	 * @return
	 */
	protected RGBA sampleRay(final List<? extends ILight> lights, final Vec3 camPos, final float deltaX, final float deltaY, final Vec3 lookVector, final Vec3 upVector,
			final Vec3 sideVector, final int j, final int i) {
		if (supersamlingRate == 0) {
			RGBA samplingColor = RGBA.BLACK;
			final Vec3 x = sideVector.scale((i) * deltaX);
			final Vec3 y = upVector.scale((j) * deltaY);
			final Vec3 dir = lookVector.add(x).add(y);

			final Line ray = new Line(camPos, dir);
			samplingColor = samplingColor.addRGBA(castRay(ray, lights, 0));
			return samplingColor.clamp();
		} else {
			RGBA samplingColor = RGBA.BLACK;

			final float sampleOffset = 1f / (supersamlingRate + 1);

			for (int k = -supersamlingRate; k <= supersamlingRate; ++k) {
				for (int l = -supersamlingRate; l <= supersamlingRate; ++l) {
					final Vec3 x = sideVector.scale((i + k * sampleOffset) * deltaX);
					final Vec3 y = upVector.scale((float) ((double) (j + l * sampleOffset) * (double) deltaY));
					final Vec3 dir = lookVector.add(x).add(y);

					final Line ray = new Line(camPos, dir);

					final float weight = (2 * supersamlingRate + 1) * (2 * supersamlingRate + 1);
					samplingColor = samplingColor.addRGBA(castRay(ray, lights, 0).divideRGBA(weight));
				}
			}
			return samplingColor.clamp();
		}
	}

	private RGBA castRay(final Line ray, final List<? extends ILight> lights, final int bounce) {
		if (bounce >= MAX_BOUNCES) {
			return RGBA.BLACK;
		}

		if (DEBUG)
			raysCast.incrementAndGet();

		// find nearest intersection point in scene
		final Optional<IntersectionResult> nearestIntersectionOption = findNearestIntersection(ray);

		// no object intersection found
		if (!nearestIntersectionOption.isPresent()) {
			return RGBA.BLACK;
		}
		final IntersectionResult nearestIntersection = nearestIntersectionOption.get();
		final Vec3 normal = nearestIntersection.normal;
		final RayTracingObject intersectedObject = nearestIntersection.object;

		// shade
		RGBA finalColor = new RGBA(0, 0, 0, 0);

		// check if path to light is clear (otherwise shadow)
		for (final ILight light : lights) {
			final Vec3 shadedColor = intersectedObject.shade(nearestIntersection.position, ray.getOrigin(), normal, light, w, h);
			final RGBA color = new RGBA(shadedColor.x, shadedColor.y, shadedColor.z, 1);
			final Vec3 positionOnSurface = nearestIntersection.position.add(normal.scale(EPSILON));
			final float distanceToLight = light.getPosition().subtract(nearestIntersection.position).length();
			final boolean isVisible = isVisible(light, positionOnSurface, distanceToLight);

			// soft shadows
			final GenericLight gl = (GenericLight) light;

			switch (gl.getLightSource().getType()) {
			case AREA_LIGHT:
				final AreaLight alight = (AreaLight) light;
				if (shadowSamplingRate <= 0) {
					finalColor = addAmbientColor(finalColor, color, isVisible);
				} else {
					finalColor = calculateSoftShadows(nearestIntersection, finalColor, light, color, positionOnSurface, (alight.width) / shadowSamplingRate, (alight.height)
							/ shadowSamplingRate);
				}
				break;
			case POINT_LIGHT:
				final PointLight plight = (PointLight) light;
				if (plight.radius <= 1 || shadowSamplingRate <= 0) {
					finalColor = addAmbientColor(finalColor, color, isVisible);
				} else {
					finalColor = calculateSoftShadows(nearestIntersection, finalColor, light, color, positionOnSurface, (plight.radius) / shadowSamplingRate, (plight.radius)
							/ shadowSamplingRate);
				}
				break;
			default:
				// add ambient color
				finalColor = addAmbientColor(finalColor, color, isVisible);
			}

		}
		final IRayTracingMaterial material = intersectedObject.getRayTracingMaterial();

		float reflection = material.getReflection();

		// refract
		if (material.isTransparent()) {
			final Vec3 N = normal.normalize();
			final Vec3 normalizedDirection = ray.getDirection().normalize();

			final float n1;
			final float n2;
			final float cosI;

			if (N.dot(normalizedDirection) < 0) {
				n1 = 1;
				n2 = material.getRefractiveIndex();
				cosI = -N.dot(normalizedDirection);
			} else {
				n1 = material.getRefractiveIndex();
				n2 = 1;
				cosI = N.dot(normalizedDirection);
			}

			final float n = n1 / n2;

			// fresnel equation
			final double sinT2 = n * n * (1.0 - cosI * cosI);

			if (sinT2 > 1) {
				// total internal reflection.
				reflection = 1.0f;
			} else {
				final double cosT = Math.sqrt(1.0 - sinT2);
				final double r0rth = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
				final double rPar = (n2 * cosI - n1 * cosT) / (n2 * cosI + n1 * cosT);

				reflection = (float) ((r0rth * r0rth + rPar * rPar) / 2.0);
			}

			final float refraction = 1 - reflection;

			if (refraction > 0) {
				// refraction
				final float cosT = (float) Math.sqrt(1 - n * n * (1 - cosI * cosI));
				final Vec3 newRayDir = ray.getDirection().scale(n).add(N.scale(n * cosI - cosT));
				final Vec3 newRayPos = nearestIntersection.position.add(newRayDir.normalize().scale(EPSILON));
				final Line refractedRay = new Line(newRayPos, newRayDir);
				finalColor = finalColor.addRGBA(castRay(refractedRay, lights, bounce + 1).scaleRGBA(refraction));
			}
		}

		// reflect
		if (reflection > 0) {
			final Vec3 incident = nearestIntersection.position.subtract(ray.getOrigin());
			final Line reflecteRay = reflect(incident, nearestIntersection);
			finalColor = finalColor.addRGBA(castRay(reflecteRay, lights, bounce + 1).scaleRGBA(reflection));
		}

		return finalColor;
	}

	private RGBA addAmbientColor(RGBA finalColor, final RGBA color, final boolean isVisible) {
		if (!isVisible) {
			finalColor = finalColor.addRGBA(color.scaleRGBA(AMBIENT_FACTOR));
		} else {
			finalColor = finalColor.addRGBA(color);
		}
		return finalColor;
	}

	private RGBA calculateSoftShadows(final IntersectionResult nearestIntersection, RGBA finalColor, final ILight light, final RGBA color, final Vec3 positionOnSurface, final float xsize, final float ysize) {
		float scale = 0f;
		for (int x = 0; x < shadowSamplingRate; x++) {
			for (int y = 0; y < shadowSamplingRate; y++) {
				final Vec3 lp = new Vec3(light.getPosition().x + xsize * x, light.getPosition().y, light.getPosition().z + y * ysize);
				final float a_dist = lp.subtract(nearestIntersection.position).length();
				if (isVisible(lp, positionOnSurface, a_dist)) {
					scale++;
				}
			}
		}

		if (scale > 0) {
			finalColor = finalColor.addRGBA(color.scaleRGBA(scale / (shadowSamplingRate * shadowSamplingRate)));
		}
		return finalColor;
	}

	private boolean isVisible(final ILight light, final Vec3 positionOnSurface, final float distanceToLight) {
		return isVisible(light.getPosition(), positionOnSurface, distanceToLight);
	}

	private boolean isVisible(final Vec3 light, final Vec3 positionOnSurface, final float distanceToLight) {

		final Line lightRay = new Line(positionOnSurface, light.subtract(positionOnSurface));

		for (final RayTracingObject object : objects) {
			if (DEBUG)
				intersectionChecks.incrementAndGet();
			final Optional<IntersectionResult> intersectionOption = object.intersect(lightRay);

			if (intersectionOption.isPresent() && !intersectionOption.get().object.getRayTracingMaterial().isTransparent() && intersectionOption.get().dist < distanceToLight) {
				return false;
			}
		}
		return true;
	}

	private Optional<IntersectionResult> findNearestIntersection(final Line currentRay) {

		Optional<IntersectionResult> nearestIntersectionOption = Optional.empty();
		for (final RayTracingObject r : objects) {
			final Optional<IntersectionResult> intersectionOption = r.intersect(currentRay);

			if (DEBUG)
				intersectionChecks.incrementAndGet();

			if (intersectionOption.isPresent()) {
				final IntersectionResult intersectionResult = intersectionOption.get();

				if (nearestIntersectionOption.isPresent()) {
					final IntersectionResult currentNearest = nearestIntersectionOption.get();

					// found new nearest
					if (intersectionResult.dist < currentNearest.dist) {
						nearestIntersectionOption = intersectionOption;
					}
				} else {
					nearestIntersectionOption = intersectionOption;
				}
			}
		}
		return nearestIntersectionOption;
	}

	private static IMesh createScreenPlane(final float x, final float y, final float w, final float h, final Texture texture) {
		final IAttribute[] attribs = { IMaterial.POSITION_ARRAY, IMaterial.COLOR_MAP_ARRAY };
		final float[] position = { x, y, 0, x + w, y, 0, x + w, y + h, 0, x, y, 0, x + w, y + h, 0, x, y + h, 0 };
		final float[][] data = { position, MeshLibrary.DEFAULT_QUAD_TEX_COORDS };
		final IGeometry geometry = new DefaultGeometry(Primitive.TRIANGLES, attribs, data);

		return new DefaultMesh(new ColorMapMaterial(texture), geometry, Pass.DEVICE_SPACE_OVERLAY);
	}

	@Override
	public void addLight(final ILight light) {

	}

	@Override
	public void removeLight(final ILight light) {

	}

	/**
	 * Renders the actual scene.
	 * 
	 * @param light
	 * @param camPos
	 * @param deltaX
	 * @param deltaY
	 * @param lookVector
	 * @param upVector
	 * @param sideVector
	 */
	protected abstract void internalRender(List<? extends ILight> lights, Vec3 camPos, float deltaX, float deltaY, Vec3 lookVector, Vec3 upVector, Vec3 sideVector);

}
