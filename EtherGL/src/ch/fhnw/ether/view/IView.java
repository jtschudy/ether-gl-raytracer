/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */package ch.fhnw.ether.view;

import java.util.EnumSet;

import javax.media.opengl.GLAutoDrawable;

import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.scene.camera.CameraMatrices;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.util.IUpdateListener;
import ch.fhnw.util.Viewport;
import ch.fhnw.util.math.Mat4;

/**
 * A 'view' here is a view with some control functionality, i.e. it handles the rendering of the model and also the user
 * input specific to the view.
 * 
 * @author radar
 */
public interface IView extends IUpdateListener {
	enum ViewType {
		INTERACTIVE_VIEW, MAPPED_VIEW
	}
	
	enum ViewFlag {
		/** Grid visibility in navigation tool. */
		GRID, 
	}

	public static final class Config {
		private final ViewType          viewType;
		private final int               fsaaSamples;
		private final EnumSet<ViewFlag> flags;
		
		public Config(ViewType viewType, int fsaaSamples, ViewFlag ...flags) {
			this.viewType    = viewType;
			this.fsaaSamples = fsaaSamples;
			this.flags = EnumSet.noneOf(ViewFlag.class);
			for(ViewFlag flag : flags)
				this.flags.add(flag);
		}
		
		public ViewType getViewType() {
			return viewType;
		}
		
		public int getFSAASamples() {
			return fsaaSamples;
		}

		public boolean has(ViewFlag flag) {
			return flags.contains(flag);
		}
	}
	
	Config INTERACTIVE_VIEW = new Config(ViewType.INTERACTIVE_VIEW, 0, ViewFlag.GRID);
	Config MAPPED_VIEW      = new Config(ViewType.MAPPED_VIEW,      0, ViewFlag.GRID);
	
	/**
	 * Dispose this view and release all associated resources
	 */
	void dispose();
	
	/**
	 * Get the drawable associated to this view.
	 * @return the drawable
	 */
	GLAutoDrawable getDrawable();

	/**
	 * Get the controller this view belongs to.
	 * 
	 * @return the controller
	 */
	IController getController();

	/**
	 * Get associated camera.
	 * 
	 * @return the camera
	 */
	ICamera getCamera();

	/**
	 * Set associated camera.
	 * 
	 * @param camera
	 *            camera to be set
	 */
	void setCamera(ICamera camera);

	/**
	 * Calculate camera matrices (if camera changed) and return them.
	 * 
	 * @return the camera matrices
	 */
	CameraMatrices getCameraMatrices();

	/**
	 * Set camera matrices to given fixed view and projection matrix. Will lock matrices and disable camera control. Use
	 * setCameraMatrices(null, null) to unlock and re-enable camera control.
	 * 
	 * @param viewMatrix
	 *            the view matrix to be set
	 * @param projMatrix
	 *            the projection matrix to be set
	 */
	void setCameraMatrices(Mat4 viewMatrix, Mat4 projMatrix);

	/**
	 * Get viewport [x, y, w, h].
	 * 
	 * @return the viewport
	 */
	Viewport getViewport();

	/**
	 * Get view configuration.
	 * 
	 * @return the view configuration
	 */
	Config getConfig();

	/**
	 * Check whether view is enabled for rendering.
	 * 
	 * @return true if view is enabled, false otherwise
	 */
	boolean isEnabled();

	/**
	 * Enable or disable view for rendering.
	 * 
	 * @param enabled
	 *            enables view if true, disables otherwise
	 */
	void setEnabled(boolean enabled);

	/**
	 * Check whether view currently receives events.
	 * 
	 * @return true if view receives events, false otherwise
	 */
	boolean isCurrent();

	/**
	 * Request to repaint this view.
	 */
	void repaint();
}
