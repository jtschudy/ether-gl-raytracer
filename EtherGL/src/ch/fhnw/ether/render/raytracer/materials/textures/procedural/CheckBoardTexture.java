package ch.fhnw.ether.render.raytracer.materials.textures.procedural;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

public class CheckBoardTexture extends ProceduralTexture {

	private final int squareSize;
	private final int offset;
	private final RGB color1;
	private final RGB color2;

	public CheckBoardTexture(int squareSize, int scale) {
		this(squareSize, scale, 0, RGB.BLACK, RGB.WHITE);
	}

	public CheckBoardTexture(int squareSize, int scale, int offset, RGB color1, RGB color2) {
		super(scale);
		this.squareSize = squareSize;
		this.offset = offset;
		this.color1 = color1;
		this.color2 = color2;
	}

	@Override
	public RGB getPixel(Vec3 origin, Vec2 p, int w, int h) {
		int x = (int) ((p.x + w + offset) * scale / squareSize); // + w to avoid texture being in center
		int y = (int) ((p.y + h + offset) * scale / squareSize); // + h to avoid texture being in center
		return x % 2 == 0 ^ y % 2 == 0 ? color1 : color2;
	}
}