/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */package ch.fhnw.ether.examples.raytracing;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.EventDrivenScheduler;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.RayTracingTexture;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.MarbleTexture;
import ch.fhnw.ether.render.raytracer.objects.parametric.ParametricRayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.PlaneSurface;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.SphereSurface;
import ch.fhnw.ether.render.raytracer.renderers.AbstractRayTracingRenderer;
import ch.fhnw.ether.render.raytracer.renderers.ParallelRayTracingRenderer;
import ch.fhnw.ether.render.raytracer.shaders.PhongShader;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.I3DObject;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.PointLight;
import ch.fhnw.ether.view.IView;
import ch.fhnw.ether.view.gl.DefaultView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public class RefractionExample1 {

	static {
		System.setProperty("jogamp.common.utils.locks.Lock.timeout", "10000000");
	}

	public static void main(final String[] args) {
		new RefractionExample1();
	}

	public static int r(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}

	public static float r2(int min, int max) {
		return (float) (Math.random() < 0.5 ? ((1 - Math.random()) * (max - min) + min) : (Math.random() * (max - min) + min));
	}

	public RefractionExample1() {
		final int superSampling = 0;
		final int softShadowSampling = 0;

		// load objects
		final List<I3DObject> objects = new ArrayList<>();

		// setup scene
		objects.add(new PointLight(new Vec3(1000, -500, 3000), RGB.BLACK, RGB.WHITE));
		objects.add(new PointLight(new Vec3(-1000, -500, 3000), RGB.BLACK, RGB.WHITE));

		final int squareSize = 700;
		final int minSphere = 150;
		final int maxSphere = 400;

		for (int j = 0; j < 11; j++) {
			for (int i = 0; i < 11; i++) {
				if (r(0, 1) == 1)
					objects.add(new ParametricRayTracingObject(new Vec3(j * squareSize, i * squareSize, 100), new SphereSurface(r(minSphere, maxSphere)), new PhongShader(), getRandomMaterial()));
				if (r(0, 1) == 1)
					objects.add(new ParametricRayTracingObject(new Vec3(-j * squareSize, i * squareSize, 100), new SphereSurface(r(minSphere, maxSphere)), new PhongShader(), getRandomMaterial()));
				if (r(0, 1) == 1)
					objects.add(new ParametricRayTracingObject(new Vec3(-j * squareSize, -i * squareSize, 100), new SphereSurface(r(minSphere, maxSphere)), new PhongShader(), getRandomMaterial()));
				if (r(0, 1) == 1)
					objects.add(new ParametricRayTracingObject(new Vec3(j * squareSize, -i * squareSize, 100), new SphereSurface(r(minSphere, maxSphere)), new PhongShader(), getRandomMaterial()));
			}
		}

		objects.add(new ParametricRayTracingObject(new Vec3(1000, -450, 4000), new SphereSurface(400), new PhongShader(), new GlassMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-1000, -450, 4000), new SphereSurface(400), new PhongShader(), new GlassMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, -450, 4000), new SphereSurface(400), new PhongShader(), new GlassMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(1000, 450, 4000), new SphereSurface(400), new PhongShader(), new GlassMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-1000, 450, 4000), new SphereSurface(400), new PhongShader(), new GlassMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 450, 4000), new SphereSurface(400), new PhongShader(), new GlassMaterial()));

		objects.add(new ParametricRayTracingObject(new Vec3(0, 0, 0), new PlaneSurface(), new PhongShader(), new TileMaterial()));

		// create controller, camera, scene and view
		final AbstractRayTracingRenderer renderer = new ParallelRayTracingRenderer(superSampling, softShadowSampling);
		final IController controller = new DefaultController(new EventDrivenScheduler(), renderer);
		final IScene scene = new DefaultScene(controller);
		controller.setScene(scene);

		objects.stream().forEach(so -> scene.add3DObject(so));

		final Vec3 cameraPosition = new Vec3(0, 0, 5200);
		final Vec3 targetPosition = new Vec3(0, 0, 0);
		final Vec3 look = targetPosition.subtract(cameraPosition).normalize();
		final Vec3 rightVector = look.cross(Vec3.Y);
		final Vec3 up = look.cross(rightVector).scale(-1);
		final ICamera camera = new Camera(cameraPosition, targetPosition, up, 2.5f, 0.5f, Float.POSITIVE_INFINITY);
		final IView view = new DefaultView(controller, 50, 50, 800, 600, IView.MAPPED_VIEW, ExampleUtil.getWindowTitle(getClass().getName(), superSampling, softShadowSampling), camera);
		controller.addView(view);
	}

	private IRayTracingMaterial getRandomMaterial() {
		IRayTracingMaterial rndMat = null;
		switch (r(0, 4)) {
		case 0:
			rndMat = new ReflectiveMaterial();
			break;
		case 1:
			rndMat = new MattMaterial2(new RGB(r2(0, 1), r2(0, 1), r2(0, 1)));
			break;
		case 2:
			rndMat = new MattMaterial(new RGB(r2(0, 1), r2(0, 1), r2(0, 1)));
			break;
		case 3:
			rndMat = new MarbleMaterial();
			break;
		case 4:
			rndMat = new EarthMaterial();
			break;
		default:
			rndMat = null;
		}
		return rndMat;
	}

	private static class TileMaterial implements IRayTracingMaterial {
		private static RayTracingTexture texture;
		final static URL resource = EarthMaterial.class.getClassLoader().getResource("ch/fhnw/ether/examples/raytracing/tile1.jpg");

		public TileMaterial() {
			texture = new RayTracingTexture(resource, false);
		}

		@Override
		public float getRefractiveIndex() {
			return 1;
		}

		@Override
		public float getReflection() {
			return 0;
		}

		@Override
		public boolean isTransparent() {
			return false;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(0, 0, 0f);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return texture;
		}

		@Override
		public NormalMap getNormalMap() {
			return null;
		}
	}

	private static class EarthMaterial implements IRayTracingMaterial {
		private static RayTracingTexture texture;
		final static URL resource = EarthMaterial.class.getClassLoader().getResource("ch/fhnw/ether/examples/raytracing/earth_nasa.jpg");

		public EarthMaterial() {
			texture = new RayTracingTexture(resource, true);
		}

		@Override
		public float getRefractiveIndex() {
			return 1;
		}

		@Override
		public float getReflection() {
			return 0;
		}

		@Override
		public boolean isTransparent() {
			return false;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(0, 0, 0f);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return texture;
		}

		@Override
		public NormalMap getNormalMap() {
			return null;
		}
	}

	private static class ReflectiveMaterial implements IRayTracingMaterial {
		final SimpleColorTexture colorTexture = new SimpleColorTexture(new RGB(0f, 0f, 0f));

		@Override
		public float getRefractiveIndex() {
			return 1f;
		}

		@Override
		public float getReflection() {
			return 0.4f;
		}

		@Override
		public boolean isTransparent() {
			return false;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(1, 1f, 1f);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return colorTexture;
		}

		@Override
		public NormalMap getNormalMap() {
			return null;
		}

	}

	private static class MattMaterial2 implements IRayTracingMaterial {

		final SimpleColorTexture colorTexture;

		public MattMaterial2(final RGB rgb) {
			colorTexture = new SimpleColorTexture(rgb);
		}

		@Override
		public float getRefractiveIndex() {
			return 1f;
		}

		@Override
		public float getReflection() {
			return 0.05f;
		}

		@Override
		public boolean isTransparent() {
			return false;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(0, 0f, 0.2f);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return colorTexture;
		}

		@Override
		public NormalMap getNormalMap() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	private static class MattMaterial implements IRayTracingMaterial {

		final SimpleColorTexture colorTexture;

		public MattMaterial() {
			this(new RGB(0.2f, 0.2f, 0.6f));
		}

		public MattMaterial(RGB rgb) {
			colorTexture = new SimpleColorTexture(rgb);
		}

		@Override
		public float getRefractiveIndex() {
			return 1f;
		}

		@Override
		public float getReflection() {
			return 0.03f;
		}

		@Override
		public boolean isTransparent() {
			return false;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(0, 0f, 0.2f);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return colorTexture;
		}

		@Override
		public NormalMap getNormalMap() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	private static class GlassMaterial implements IRayTracingMaterial {

		final SimpleColorTexture simpleColorTexture = new SimpleColorTexture(new RGB(0, 0, 0));

		@Override
		public float getRefractiveIndex() {
			return 1.5f;
		}

		@Override
		public float getReflection() {
			return 0f;
		}

		@Override
		public boolean isTransparent() {
			return true;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(0, 0, 0);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return simpleColorTexture;
		}

		@Override
		public NormalMap getNormalMap() {
			// TODO Auto-generated method stub
			return null;
		}

	}

	private static class MarbleMaterial implements IRayTracingMaterial {

		private final MarbleTexture texture;
		private NormalMap normalMap;

		public MarbleMaterial() {
			texture = new MarbleTexture(0.5f);
			normalMap = new NormalMap(0.5f, 0.25f);
		}

		@Override
		public float getRefractiveIndex() {
			return 1;
		}

		@Override
		public float getReflection() {
			return 0.2f;
		}

		@Override
		public boolean isTransparent() {
			return false;
		}

		@Override
		public float getShininess() {
			return 1f;
		}

		@Override
		public RGB getSpecular() {
			return new RGB(1, 1, 1f);
		}

		@Override
		public IRayTracingTexure getTexture() {
			return texture;
		}

		@Override
		public NormalMap getNormalMap() {
			return normalMap;
		}

	}

}
