package ch.fhnw.ether.render.raytracer.materials.textures.procedural;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

// http://web.eecs.umich.edu/~sugih/courses/eecs487/lectures/26-BumpMap+ProcTex.pdf
// http://lodev.org/cgtutor/randomnoise.html
public class MarbleTexture extends ProceduralTexture {

	private RGB color1 = new RGB(0.25f, 0.25f, 0.2f);
	private RGB color2 = new RGB(0.65f, 0.75f, 0.85f);

	public MarbleTexture(float scale) {
		super(scale);
	}

	@Override
	public RGB getPixel(Vec3 origin, Vec2 mapped, int w, int h) {
		double x = origin.x * scale;
		double y = origin.y * scale;
		double z = origin.z * scale;
		double noiseCoef = 0;

		for (int level = 1; level < 10; level++) {
			noiseCoef += (1.0f / level) * Math.abs(PerlinNoise.noise(level * 0.05 * x, level * 0.15 * y, level * 0.05 * z));
		}
		noiseCoef = 0.5f * Math.sin((x + y) * 0.05f + noiseCoef) + 0.5f;

		return color1.scaleRGB((float) noiseCoef).addRGB(color2).scaleRGB((float) (1 - noiseCoef));
	}

}
