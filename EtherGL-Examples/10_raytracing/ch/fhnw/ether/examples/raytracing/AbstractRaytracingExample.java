package ch.fhnw.ether.examples.raytracing;

public abstract class AbstractRaytracingExample {
	
	static {
		System.setProperty("jogamp.common.utils.locks.Lock.timeout", "10000000");
	}
	
	public int getResolutionX() {
		return 800;
	}
	
	public int getResolutionY() {
		return 600;
	}
	
	public int getSoftShadowRate() {
		return 0;
	}
	
	public int getSuperSamplingRate() {
		return 0;
	}
	
}
