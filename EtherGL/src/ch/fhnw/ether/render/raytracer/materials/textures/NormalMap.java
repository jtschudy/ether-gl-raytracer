package ch.fhnw.ether.render.raytracer.materials.textures;

import ch.fhnw.ether.render.raytracer.materials.textures.procedural.PerlinNoise;
import ch.fhnw.util.math.Vec3;

public class NormalMap {
	private float scale;
	private float amount;

	public NormalMap(float scale, float amount) {
		super();
		this.scale = scale;
		this.amount = amount;
	}

	public Vec3 modifyNormal(Vec3 normal, Vec3 p) {
		double x = p.x / scale;
		double y = p.y / scale;
		double z = p.z / scale;

		final Vec3 noise = new Vec3((float) (PerlinNoise.noise(x, y, z)), (float) (PerlinNoise.noise(y, z, x)), (float) (PerlinNoise.noise(z, x, y)));

		return normal.add(noise.scale(amount)).normalize();
	}
}
