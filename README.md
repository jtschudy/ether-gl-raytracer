Ether-GL
========

General-purpose Java / OpenGL framework for 3D object editing, visualization and projection mapping.

Overview
--------

Soon...


Repository
----------

The repository contains an Eclipse project, including dependencies such as JOGL etc. Thus the code should run out of the box on Mac OS X, Windows and Linux.


Further Info & Contact
----------------------

For questions etc. feel free to be in touch with me (Stefan Müller Arisona) at robot@arisona.ch


Credits
-------

Stefan Müller Arisona

Simon Schubiger

Samuel von Stachelski

Contributions by: Eva Friedrich